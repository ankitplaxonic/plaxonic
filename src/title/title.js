export default function titleGenrator(title) {
  document.title = `Plaxonic | ${title}`;
}
