import React from "react";
import "./styles.scss";

const Left = ({ onClick, arrowClass, arrow }) => {
  return <i className={`slider-arrow fa ${arrow ? arrow : ""} ${arrowClass ? arrowClass : ""}`} aria-hidden="true" onClick={onClick}></i>;
};

export default Left;
