import React from "react";
import "./styles.scss";

const Right = ({ onClick, arrowClass, arrow }) => {
  return <i className={`slider-arrow arrow fa  ${arrow ? arrow : ""} ${arrowClass ? arrowClass : ""}`} aria-hidden="true" onClick={onClick}></i>;
};

export default Right;
