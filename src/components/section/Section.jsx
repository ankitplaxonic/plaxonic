import React from "react";
import "./style.scss";

const Section = ({ children, background, customClass }) => {
  if (background)
    return (
      <div className={`section ${customClass ? customClass : ""}`} style={{ backgroundImage: `url(${background})` }}>
        {children}
      </div>
    );

  return <div className={`section ${customClass ? customClass : ""}`}>{children}</div>;
};

export default Section;
