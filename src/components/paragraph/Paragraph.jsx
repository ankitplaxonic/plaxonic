import React from "react";
import "./styles.scss";

const Paragraph = ({ children }) => {
  return <p className="para">{children}</p>;
};

export default Paragraph;
