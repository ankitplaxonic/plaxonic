import React from "react";
import "./styles.scss";

const Usercard = ({ user, customClass }) => {
  return (
    <div className={`user-card ${customClass ? customClass : ""}`}>
      <div className="img-block">
        <img src={user.img} alt="" className="front-img" />
        <img src={user.bgImg} alt="" className="back-img" />
      </div>
      <div className="text-block">
        <h2 className="name" style={{ color: user.color }}>
          <span className="first-name">{user.firstName}</span>
          <span className="last-name">{user.lastName}</span>
        </h2>
        <h4 className="position">{user.position}</h4>
        <div className="view-btn">
          <span className="text">View bio</span> <i className="fa-fa-trash"></i>
        </div>
      </div>
    </div>
  );
};

export default Usercard;
