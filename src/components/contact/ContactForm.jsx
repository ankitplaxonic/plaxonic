import React from "react";
import images from "../../assets/images/images";
import Title from "../title/Title";
import "./styles.scss";

const ContactForm = ({ background, start, end, color }) => {
  return (
    <div className="contact">
      <div className="block">
        <img src={images.common.contactImg} alt="" className="for-ground" />
        <svg id="background" x="0px" y="0px" viewBox="0 0 1352.8 705.2">
          <linearGradient id="linear-gradient" gradientUnits="userSpaceOnUse" x1="500" y1="0" x2="1220" y2="300">
            <stop offset="0" stopColor={start ? start : "#704db8"}></stop>
            <stop offset="1" stopColor={end ? end : "#1d27cd"}></stop>
          </linearGradient>
          <path
            className="st0"
            style={{ fill: "url(#linear-gradient)" }}
            d="M67.4,331.2c-1-8,0.9-20.2,1.4-22.7c4.2-22.4,13.2-42.9,24.5-62.4c14.7-25.2,33.4-47.1,55.2-66.7
    c18.2-16.4,37.1-31.9,57.3-45.7c31.8-21.6,66.8-36.7,102.8-49.3c59.6-20.9,121-33.8,183.6-41.4c32.9-4,65.8-6.7,98.9-7.4
    c34.8-0.8,69.6-0.9,104.4,0.4c22.6,0.8,45.2,1.7,67.8,3.1c17.8,1.1,35.5,2.6,53.1,4.4c25.8,2.7,51.4,6.3,77,10.6
    c47.5,8,94.2,19.2,140,33.8c36.5,11.6,72.1,25.7,107.2,41.1c26.5,11.7,52.7,24,74.8,43.5c27.6,24.2,48.5,52.8,61.2,87.4
    c7,19.2,9.7,39,8.9,59.3c-0.6,7-2,13.8-3.9,20.5c-3.8,12.9-10.8,23.9-20,33.6c-17.6,18.6-37,35.5-53.4,55.4
    c-14.4,17.5-18.4,36.4-9.1,57.7c4.2,9.7,10.2,18.3,16,27c9.4,14.2,17.9,28.9,21.7,45.8c3.8,17,1.6,32.8-10.1,46.4
    c-11.4,13.2-25.7,22.1-42,27.8c-24.2,8.4-49.4,12-74.7,15c-27.3,3.3-54.6,5.3-82,5.6c-26,0.3-51.9,0.7-77.9-0.6
    c-17.1-0.9-34.2-2-51.3-3c-25.6-1.6-50.9-5-76.4-7.5c-24.8-2.4-49.7-4.7-74.6-5.4c-31.1-0.9-62.2-1.3-93.3,1.5
    c-28.2,2.5-56.2,6.3-83.5,13.9c-45.4,12.8-91.7,17.3-138.6,17.2c-10,0-19.9-0.2-29.9-0.7c-19.1-1-38.1-2.9-56.9-6.3
    c-24-4.4-47.3-10.9-68.5-23.6c-25.9-15.5-38.4-40.2-33.3-67.4c4.4-23.5,9.6-46.9,13.2-70.6c2.3-15,3.3-30.1,2.3-45.3
    c-0.7-10.6-3.7-20.6-10.4-29.2c-8.5-11-20.6-16-33.3-19.7c-18.4-5.4-37.6-6.5-56.4-10c-20.3-3.9-40.4-8.7-58.6-19
    c-12.5-7-22.4-16.3-28.4-29.4C69.7,343.2,68.4,339.2,67.4,331.2z"
          ></path>
        </svg>
        {/* <img src={background} alt="" className="background" /> */}
        <div className="form-block">
          <Title frontText="Drop Us A Line" backText="Contact" frontColor="#fff" backColor="#2e31f6" />
          <p className="desc">Hello! Your name please?</p>
          <input className="name-input" type="text" name="" id="" placeholder="Full Name" />
          <div className="btn-box">
            <input type="submit" className="next-btn" value="Next" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactForm;
