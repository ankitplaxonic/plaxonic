import React from "react";
import "../styles.scss";

const PlayBtn = ({ customClass, children, onclick }) => {
  return (
    <button className={`play-btn${customClass ? customClass : ""}`} onClick={onclick ? onclick : undefined}>
      <span> {children}</span>
    </button>
  );
};

export default PlayBtn;
