import React from "react";
import { Link } from "react-router-dom";
import "../styles.scss";

const ButtonDefault = ({ link, customClass, path, name, button }) => {
  if (link)
    return (
      <Link className={`default-link ${customClass ? customClass : ""}`} to={path}>
        <svg>
          <rect x="0" y="0" fill="none" width="100%" height="100%"></rect>
        </svg>
        {name}
      </Link>
    );

  return <button className={`default-btn ${customClass ? customClass : ""}`}>{name}</button>;
};

export default ButtonDefault;
