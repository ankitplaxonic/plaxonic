import React from "react";
import "./styles.scss";

const Mainsection = ({ children, customClass }) => {
  return <div className={`main-sec ${customClass ? customClass : ""}`}>{children}</div>;
};

export default Mainsection;
