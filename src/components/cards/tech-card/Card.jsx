import React from "react";
import "./styles.scss";

const Card = ({ img, title }) => {
  return (
    <div className="tech-card">
      <div className="img-wrapper">
        <img src={img} alt="" />
      </div>
      <p>{title}</p>
    </div>
  );
};

export default Card;
