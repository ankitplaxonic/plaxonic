import React from "react";

const SubTitle = ({ title }) => {
  return <h4 className="sub-title">{title}</h4>;
};

export default SubTitle;
