import React from "react";
import "./style.scss";

const Title = ({ frontText, backText, frontColor, backColor }) => {
  return (
    <h2 className="title-default" style={{ color: frontColor }}>
      <span className="title-pre" style={{ color: backColor }}>
        {backText}
      </span>
      {frontText}
    </h2>
  );
};

export default Title;
