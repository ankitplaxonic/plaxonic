import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { motion } from "framer-motion";
import Header from "./containers/header/Header";
import Footer from "./containers/footer/Footer";
import routes from "./routes/routes";
import "./styles.scss";

const App = ({ isSubOpen }) => {
  const routesList = routes.map((route) => <Route key={route.id} exact={route.exact} path={route.path} component={route.component} />);

  return (
    <motion.div
      className={`main ${isSubOpen ? "sub-open" : ""}`}
      animate={isSubOpen ? { scaleX: 0.95 } : { scaleX: 1 }}
      transition={{ duration: 0.3 }}
    >
      <Header />

      <Switch>
        {routesList}
        <Redirect from="*" to="not-found" />
      </Switch>
      <Footer />
    </motion.div>
  );
};

const mapStateToProp = (state) => ({ isSubOpen: state.menu.isSubOpen });

export default connect(mapStateToProp)(App);
