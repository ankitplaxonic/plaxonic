import React from "react";
import Section from "../../components/section/Section";
import FooterList from "./helpers/FooterList";
import dataFooter from "./dataFooter";
import images from "../../assets/images/images";
import { Link } from "react-router-dom";
import "./styles.scss";

const Footer = () => {
  return (
    <footer style={{ backgroundImage: `url(${images.footer.backgroundImg})` }}>
      <Section customClass="footer-sectoin">
        <div className="footer-wrapper">
          <div className="block">
            <div className="img-wrapper">
              <Link to="/">
                <img src={images.footer.logoBlack} alt="" />
              </Link>
              <Link to="/">
                <img src={images.footer.goodFirmBadge} alt="" />
              </Link>
            </div>
          </div>
          <div className="block">
            <FooterList data={dataFooter.colOne} />
          </div>
          <div className="block">
            <FooterList data={dataFooter.colTwo} />
          </div>
          <div className="block">
            <FooterList data={dataFooter.colThree} />
          </div>
        </div>
        <div className="copy-right">
          <p>© 2013-2020 Plaxonic Technologies. All Rights Reserved.</p>
        </div>
      </Section>
    </footer>
  );
};

export default Footer;
