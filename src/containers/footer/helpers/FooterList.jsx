import React from "react";
import { Link } from "react-router-dom";

const FooterList = ({ data }) => {
  const list = data.list.map((item) => (
    <li key={item.id}>
      <Link to={item.link}>{item.name}</Link>
    </li>
  ));

  return (
    <div className="box">
      <h5 className="title">{data.title}</h5>
      <ul className="list">{list}</ul>
    </div>
  );
};

export default FooterList;
