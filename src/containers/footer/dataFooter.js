const dataFooter = {
  colOne: {
    title: "Company",
    list: [
      { id: 1, name: "About Us", link: "about-us" },
      { id: 2, name: "Contact Us", link: "contact-us" },
      { id: 3, name: "Terms of Use", link: "terms-of-use" },
      { id: 4, name: "Privacy Statement", link: "privacy" },
      { id: 5, name: "Fraudulent Policy", link: "fraudulent-policy" },
    ],
  },
  colTwo: {
    title: "Services",
    list: [
      { id: 1, name: "Ai & automation", link: " digital-automation" },
      { id: 2, name: "Data Analytics ", link: "data-analytics" },
      { id: 3, name: "Data Analytics ", link: "data-analytics " },
      { id: 4, name: "SPA", link: "spa" },
      { id: 5, name: "Cloud", link: "cloud" },
    ],
  },
  colThree: {
    title: "Social",
    list: [
      { id: 1, name: "Facebook ", link: "facebook" },
      { id: 2, name: "Twitter", link: "twitter" },
      { id: 3, name: "Instagram", link: "instagram" },
      { id: 4, name: "Linkedin", link: "linkedin" },
      { id: 5, name: "Youtube", link: "youtube" },
    ],
  },
};

export default dataFooter;
