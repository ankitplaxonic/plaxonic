import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import images from "../../assets/images/images";
import Mainsection from "../../components/mainsection/Mainsection";
import Paragraph from "../../components/paragraph/Paragraph";
import SubTitle from "../../components/title/SubTitle";
import Title from "../../components/title/Title";
import constants from "../../constants/constants";
import "./styles.scss";

const NotFound = ({ handleNotFoundMenuBg }) => {
  document.title = "plaxonic | Not Found";
  useEffect(() => {
    handleNotFoundMenuBg("linear-gradient(to right, rgb(255, 151, 0), rgb(255, 71, 0))");
  }, [handleNotFoundMenuBg]);

  return (
    <div className="not-found">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.notFound.background} alt="" />
        <div className="title-block">
          <Title frontText="Oops!" backText="404" backColor="rgb(254, 240, 240)" />
          <div className="para-block">
            <SubTitle title="Something went wrong here" />
            <Paragraph>We are working on it and we'll get it fixed as soon as possible.</Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.notFound.frontImg} alt="" />
        </div>
        <Link className="back-to-home" to="/">
          Back to home
        </Link>

        <img src={images.notFound.floatingOne} alt="" className="floating-img-1 floating" />
        <img src={images.notFound.floatingTwo} alt="" className="floating-img-2 floating" />
        <img src={images.notFound.floatingThree} alt="" className="floating-img-3 floating" />
      </Mainsection>
    </div>
  );
};
const mapDispatchToProp = (dispatch) => ({
  handleNotFoundMenuBg: (menuBackground) =>
    dispatch({
      type: constants.SET_NOT_FOUND_MENU_BG,
      payload: menuBackground,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(NotFound));
