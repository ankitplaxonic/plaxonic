import React from "react";
import { connect } from "react-redux";
import images from "../../../assets/images/images";
import Mainsection from "../../../components/mainsection/Mainsection";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import Usercard from "../../../components/usercard/Usercard";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import "./styles.scss";

const Leadership = ({ leaders, setCurrentPage, location }) => {
  titleGenrator("Leadership");

  setCurrentPage(location.pathname);

  const restLeaders = leaders.splice(1, leaders.length - 1);
  const leaderCard = restLeaders.map((leader) => <Usercard key={leader.id} user={leader} customClass="leaders-card" />);

  return (
    <div className="leadership">
      <Mainsection>
        <img className="main-bg" src={images.leaderShipImg.bannerImg} alt="" />
        <div className="title-card">
          <div className="title-block">
            <Title frontText="Leadership" backText="Mentors" backColor="rgba(0, 26, 188, 0.2)" />
            <Usercard user={leaders[0]} customClass="mainUser" />
          </div>
          <div className="cards-block">card</div>
        </div>
      </Mainsection>
      <Section customClass="leaders-block">{leaderCard}</Section>
      {/* <Section>{leaderCard}</Section> */}
    </div>
  );
};

const mapStateToProp = (state) => ({
  leaders: state.leadership.leaders,
});

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});
export default connect(mapStateToProp, mapDispatchToProp)(Leadership);
