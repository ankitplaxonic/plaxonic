import React from "react";
import images from "../../../assets/images/images";
import Card from "../../../components/cards/tech-card/Card";
import SubTitle from "../../../components/title/SubTitle";

const Stack = () => {
  return (
    <div className="stack">
      <div className="row">
        <div className="col-md-6">
          <SubTitle title="Front-End Web" />
          <div className="card-row">
            {images.techstack.card.frontEndWeb.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-6">
          <SubTitle title="Front-End Mobile" />
          <div className="card-row">
            {images.techstack.card.frontEndMobile.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="Backend" />
          <div className="card-row">
            {images.techstack.card.backEnd.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="Persistence Stack" />
          <div className="card-row">
            {images.techstack.card.persist.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="Infrastructure" />
          <div className="card-row">
            {images.techstack.card.Infrastructure.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="CI & CD" />
          <div className="card-row">
            {images.techstack.card.cicd.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="Continuous Testing" />
          <div className="card-row">
            {images.techstack.card.testing.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="Infrastructure Automation" />
          <div className="card-row">
            {images.techstack.card.infrastructureAutomation.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
        <div className="col-md-12">
          <SubTitle title="Monitoring" />
          <div className="card-row">
            {images.techstack.card.monitoring.map((item) => (
              <Card key={item.id} img={item.img} title={item.name} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Stack;
