import React from "react";
import images from "../../assets/images/images";
import ButtonDefault from "../../components/buttons/buttonDefault/ButtonDefault";
import Mainsection from "../../components/mainsection/Mainsection";
import Paragraph from "../../components/paragraph/Paragraph";
import Section from "../../components/section/Section";
import SubTitle from "../../components/title/SubTitle";
import Title from "../../components/title/Title";
import Stack from "./section/Stack";
import ContactForm from "../../components/contact/ContactForm";
import "./styles.scss";
import titleGenrator from "../../title/title";

const TechStack = () => {
  titleGenrator("Tech Stack");

  return (
    <div className="tech-stack">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.techstack.mainBg} alt="" />
        <div className="title-block">
          <Title frontText="Tech Stack" backText="Technologies" backColor="rgba(231, 244, 254)" />
          <div className="para-block">
            <SubTitle title="Powering The Future With Technology!" />
            <Paragraph>
              Plaxonic’s motto of ‘Invent & Innovate’ demands leveraging the latest technologies and making the best things work for us and our
              clients. Furthermore, our developers and coders are always learning the new tech to stay updated and sharp.
            </Paragraph>
          </div>
          <ButtonDefault path="/test" link name="Our Tech Stack" />
        </div>
        <div className="img-block">
          <img src={images.techstack.techLaptop} alt="" />
        </div>
      </Mainsection>
      <Section customClass="stack-section">
        <Stack />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.common.contactBg} />
      </Section>
    </div>
  );
};

export default TechStack;
