import React, { useState, useRef, useEffect } from "react";
import images from "../../assets/images/images";
import ButtonDefault from "../../components/buttons/buttonDefault/ButtonDefault";
import PlayBtn from "../../components/buttons/playBtn/PlayBtn";
import ContactForm from "../../components/contact/ContactForm";
import Paragraph from "../../components/paragraph/Paragraph";
import Section from "../../components/section/Section";
import Title from "../../components/title/Title";
import Homeslider from "./sections/slider/Homeslider";
import SectioFour from "./sections/SectioFour";
import SectionThree from "./sections/SectionThree";
import SectionTwo from "./sections/SectionTwo";
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import "./styles.scss";
import titleGenrator from "../../title/title";
import constants from "../../constants/constants";
import { connect } from "react-redux";

const Home = ({ handleNotFoundMenuBg }) => {
  const [playing, setPlaying] = useState(false);
  const vidRef = useRef(null);
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    handleNotFoundMenuBg("linear-gradient(to right, rgb(42, 49, 191), rgb(18, 129, 216))");
    if (inView) {
      controls.start("visible");
    }
    titleGenrator("Digital Transformation | IT Solutions");
  }, [controls, inView, handleNotFoundMenuBg]);
  // console.log(playing);
  const handlePlay = () => {
    // console.log(playing);
    setPlaying(!playing);
    if (!playing) {
      return vidRef.current.play();
    }
    vidRef.current.pause();

    // console.log(playing);
  };

  return (
    <div className="home">
      <Homeslider />
      <Section>
        <div className="row">
          <div className="col-md-6">
            <motion.div
              ref={ref}
              animate={controls}
              initial="hidden"
              variants={{
                visible: { opacity: 1, x: 0 },
                hidden: { opacity: 0, x: -100 },
              }}
              transition={{ duration: 0.6 }}
              className="video-wrapper"
            >
              <PlayBtn onclick={handlePlay}>{playing ? <i className="fa fa-pause"></i> : <i className="fa fa-play"></i>}</PlayBtn>
              <img src={images.home.videoWrapper} alt="" />
              <video ref={vidRef} src="https://www.plaxonic.com/img/home-video/this-is-how-we-work.mp4"></video>
            </motion.div>
          </div>
          <div className="col-md-6">
            <Title frontText="Embracing Challenges" backText="About" fontColor="#000" backColor="#5832e6" />
            <Paragraph>
              Inventive, innovative, zealous & futuristic – we are everything that’s required to serve enterprises through advanced digital solutions
              & research. We observe the ongoing trends and analyze the upcoming technologies to transform the digital persona of clients with our
              unbeatable performance.
            </Paragraph>
            <div className="btn-wrapper">
              <ButtonDefault name="Community" path="/community" customClass="home-link" link />
              <ButtonDefault name="Culture" path="/culture" customClass="home-link" link />
            </div>
          </div>
        </div>
      </Section>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="sec-three-parent" background={images.home.secThreeBg}>
        <SectionThree />
      </Section>
      <Section>
        <SectioFour />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.common.contactBg} start="#704db8" end="#1d27cd " />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  handleNotFoundMenuBg: (menuBackground) =>
    dispatch({
      type: constants.SET_NOT_FOUND_MENU_BG,
      payload: menuBackground,
    }),
});

export default connect(null, mapDispatchToProp)(Home);
