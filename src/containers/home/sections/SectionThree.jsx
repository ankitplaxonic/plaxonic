import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import Title from "../../../components/title/Title";
import "../styles.scss";

const SectionThree = () => {
  return (
    <div className="sec-three">
      <div className="row">
        <div className="col-md-6">
          <div className="img-wrapper">
            <img src={images.home.secThreeImg} alt="" />
          </div>
        </div>
        <div className="col-md-6">
          <div className="block">
            <Title frontText="Corporate Responsibility" backText="Our" backColor="#56b5fb" />
            <Paragraph>
              Plaxonic Technologies strongly believes in being an active part of philanthropic pursuits. From supporting NGO that spreads awareness
              for importance of women education & welfare to being a source of professional training for the weaker sections of the society, we try to
              undertake every cause that has the potential to bring a favorable change in the society. Apart from lifting the youth, we also connect
              with old-age homes and orphanages to spread care & affection in the lives of as many people as we can. Spending time with them and
              fulfilling their needs is something our people love doing. Bring stability in the society through our efforts is a goal we are moving
              forward to each day.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionThree;
