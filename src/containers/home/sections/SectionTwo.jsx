import React, { useState } from "react";
import Slider from "react-slick";
import images from "../../../assets/images/images";
import Left from "../../../components/slider-arrow/Left";
import Right from "../../../components/slider-arrow/Right";
import Para from "../../../components/paragraph/Paragraph";

const Section = () => {
  const [count, setcount] = useState(0);

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    vertical: true,
    className: "center-mode",
    centerMode: true,
    centerPadding: "0px",
    verticalSwiping: true,
    prevArrow: <Left arrowClass="left-arrow" arrow="fa-long-arrow-left" />,
    nextArrow: <Right arrowClass="right-arrow" arrow="fa-long-arrow-right" />,
    afterChange: function (currentSlide) {
      setcount(currentSlide);
      // console.log("count", count);
      // console.log("after change", currentSlide);
    },
  };

  return (
    <div className="section-two">
      <div className="row">
        <div className="col-md-4">
          <div className="slider-wrapper">
            <Slider {...settings}>
              {data.map((item) => (
                <div key={item.id} className="slider-title">
                  {item.title}
                </div>
              ))}
            </Slider>
          </div>
        </div>
        <div className="col-md-8">
          <div className="detail-block" style={{ backgroundImage: `url(${images.home.secTwoBg})` }}>
            <img src={images.home.secTwoFg} alt="" className="background" />
            <img className="flying" src={data[count].img} alt="" />
            <h4 className="title">{data[count].title}</h4>
            <Para className="para">{data[count].para}</Para>
          </div>
        </div>
      </div>
    </div>
  );
};

const data = [
  {
    id: 1,
    img: images.home.sectionTwo[0].img,
    title: "Digital Transformation",
    para: `Digital solutions figure out the weak areas, turn them into strength points and
uncover extraordinary results. Execution of thriving technology on all the right
channels helps standing ahead of others in the crowded
marketplace. Done right, digital transformation enables businesses to make the
best of digital technologies for impressive customer experience.`,
  },
  {
    id: 2,
    img: images.home.sectionTwo[1].img,
    title: "AI & Automation",
    para: `Unfolding great opportunities for the businesses to successfully move ahead in
    the corporate sector, both Artificial Intelligence & automation are changing the
    face of the market world. Unleashing their potential propels
    towards productivity along with opening scope for never-before experienced
    opportunities.`,
  },
  {
    id: 3,
    img: images.home.sectionTwo[2].img,
    title: "Business Applications",
    para: `Businesses nowadays need futuristic approach to not just stay in the competition
    but to fetch advanced imperatives. Business applications have become the modern
    way to earn success in the unpredictable market. Via their
    crucial role in innovating and optimizing various business processes, they
    infuse next generation technology in the ordinary business world. `,
  },
  {
    id: 4,
    img: images.home.sectionTwo[0].img,
    title: "Digital Transformation",
    para: `Digital solutions figure out the weak areas, turn them into strength points and
uncover extraordinary results. Execution of thriving technology on all the right
channels helps standing ahead of others in the crowded
marketplace. Done right, digital transformation enables businesses to make the
best of digital technologies for impressive customer experience.`,
  },
  {
    id: 5,
    img: images.home.sectionTwo[1].img,
    title: "AI & Automation",
    para: `Unfolding great opportunities for the businesses to successfully move ahead in
    the corporate sector, both Artificial Intelligence & automation are changing the
    face of the market world. Unleashing their potential propels
    towards productivity along with opening scope for never-before experienced
    opportunities.`,
  },
  {
    id: 6,
    img: images.home.sectionTwo[2].img,
    title: "Business Applications",
    para: `Businesses nowadays need futuristic approach to not just stay in the competition
    but to fetch advanced imperatives. Business applications have become the modern
    way to earn success in the unpredictable market. Via their
    crucial role in innovating and optimizing various business processes, they
    infuse next generation technology in the ordinary business world. `,
  },
];

export default Section;
