import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import images from "../../../../assets/images/images";
import Left from "../../../../components/slider-arrow/Left";
import Right from "../../../../components/slider-arrow/Right";
import "./styles.scss";

const {
  home: { banner },
} = images;

const bannerData = [
  { id: 1, title: "Drive Your Business On the Wheels Of Digitalization", background: banner[0].bgImage, subImg: banner[0].fgImage },
  {
    id: 2,
    title: "Old-school Visitor Management System",
    para: "Turned Contemporary Via An Augmented Application",
    background: banner[1].bgImage,
    subImg: banner[1].fgImage,
  },
  {
    id: 2,
    title: "Keeping Clients Aligned With Up-to-date Technologies",
    para: "Via Our Strong Affinity For Agile Methodologies",
    link: "agile-and-devops-services",
    background: banner[2].bgImage,
    subImg: banner[2].fgImage,
  },
  {
    id: 2,
    title: "Channelizing Our Strengths & Resources For Societal Impact",
    para: "By Undertaking Philanthropic Priorities",
    link: "community",
    background: banner[3].bgImage,
    subImg: banner[3].fgImage,
  },
];

const Homeslider = () => {
  const [current, setCurrent] = useState(1);
  const bannerArrLen = banner.length;
  // console.log(banner);

  // const handlerNext = () => {
  //   setCurrent((prev) => prev + 1);
  //   if (current >= bannerArrLen) {
  //     setCurrent(0);
  //   }
  // };
  // const handlerPrev = () => {
  //   setCurrent((prev) => prev - 1);
  //   if (current < 0) {
  //     setCurrent((prev) => (prev = 0));
  //   }
  // };

  const afterChangeHandler = (currentSlide) => {
    setCurrent(currentSlide + 1);
  };

  useEffect(() => {
    // document.querySelector(".home-slider .slick-next").addEventListener("click", handlerNext);
    // document.querySelector(".home-slider .slick-prev").addEventListener("click", handlerPrev);
  }, []);
  // console.log("current", current);

  const settings = {
    // dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <Left arrowClass="left-arrow" arrow="fa-angle-left" />,
    nextArrow: <Right arrowClass="right-arrow" arrow="fa-angle-right" />,
  };

  const bannerArr = bannerData.map((banner, index) => {
    return (
      <div className="banner-block" key={banner.id}>
        <img src={banner.background} alt="" />
        <div className="banner-detail">
          <h2 className="title">{banner.title}</h2>
          <p className="desc">{banner.para}</p>
          {banner.link && (
            <Link to={banner.link} className="more-info">
              More info
              <i className="fa fa-long-arrow-right"></i>
            </Link>
          )}
        </div>
        <div className={`fg-img fg-img-${index}`}>
          <img src={banner.subImg} className={`img-${index}`} alt="" />
        </div>
      </div>
    );
  });

  return (
    <div className="home-slider">
      <Slider {...settings} afterChange={afterChangeHandler}>
        {bannerArr}
      </Slider>
      <div className="count">
        <span className="current" data-count={`0${current}`}>
          {`0${current}`}
        </span>
        <span className="total">{bannerArrLen}</span>
      </div>
      <div className="slider-strip" style={{ backgroundImage: `url(${images.home.bannerBtm})` }}></div>
    </div>
  );
};

export default Homeslider;
