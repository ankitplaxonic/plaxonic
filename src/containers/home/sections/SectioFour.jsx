import React, { useState } from "react";
import Slider from "react-slick";
import images from "../../../assets/images/images";
import Title from "../../../components/title/Title";
import Card from "../helper/Card";
import Left from "../../../components/slider-arrow/Left";
import Right from "../../../components/slider-arrow/Right";
import { motion } from "framer-motion";

const info = [
  {
    id: 1,
    location: "India",
    thumbnail: images.home.secFour[0].thumbnail,
    main: images.home.secFour[0].main,
    phone: "+91 - 120 - 4211120",
    email: "contact@plaxonic.com",
    address: "B-10, Sector 59, Noida, 201301, INDIA",
  },
  {
    id: 2,
    location: "Usa",
    phone: "+1-800-3684805",
    thumbnail: images.home.secFour[1].thumbnail,
    main: images.home.secFour[1].main,
    email: "contact@plaxonic.com",
    address: "102 Tarpon Ave, Tarpon Springs, FL 34689, US",
  },
  {
    id: 3,
    location: "Dubai",
    phone: "050 7790114",
    thumbnail: images.home.secFour[2].thumbnail,
    main: images.home.secFour[2].main,
    email: "contact@plaxonic.com",
    address: "Techno Hub 2, Dubai Silicon Oasis (DSO), Dubai, UAE",
  },
  {
    id: 4,
    location: "Hong Kong",
    phone: "+1-800-3684805",
    thumbnail: images.home.secFour[3].thumbnail,
    main: images.home.secFour[3].main,
    email: "contact@plaxonic.com",
    address: "Room 901, Centre Point, 181-185 Gloucester Road, Wan Chai,Hong Kong",
  },
];

const SectioFour = () => {
  const [count, setcount] = useState(0);

  const setting = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: <Left arrowClass="sec-four-left" arrow="fa-angle-left" />,
    nextArrow: <Right arrowClass="sec-four-right" arrow="fa-long-arrow-right" />,
    beforeChange: function (currentSlide, nextSlide) {
      console.log("before change", currentSlide, nextSlide);
    },
    afterChange: function (currentSlide) {
      setcount(currentSlide);
      console.log("count", count);
      console.log("after change", currentSlide);
    },
  };

  return (
    <div className="sec-four">
      <motion.img src={images.home.homeFlyingOne} className="flying-one" animate={{ scale: 1.5 }} transition={{ type: "spring", damping: 3 }} />
      <div className="row">
        <div className="col-md-6">
          <div className="block-container">
            <div className="block">
              <Title frontText="Offices" backText="Our" backColor="#5832e6" />
              <div className="details">
                <div className="row">
                  <div className="col-md-6">
                    <Card title="Phone" cusHref={`tel:${info[count].phone}`} value={info[count].phone} />
                  </div>
                  <div className="col-md-6">
                    <Card title="Email" cusHref={`mailto:${info[count].email}`} value={info[count].email} />
                  </div>
                  <div className="col-md-12">
                    <Card title="Addres" value={info[count].address} />
                  </div>
                </div>
              </div>
            </div>
            <div className="slider-wrapper">
              <Slider {...setting}>
                {info.map((item) => (
                  <div key={item.id} className="slider-item">
                    <img src={item.thumbnail} alt="" />
                    <h4 className="title">{item.location}</h4>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="img-block">
            <img src={info[count].main} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectioFour;
