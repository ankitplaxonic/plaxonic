import React from "react";
import "./styles.scss";

const Title = ({ title, type, cusHref, value }) => {
  if (cusHref)
    return (
      <div className="cus-card">
        <h4 className="title">{title}</h4>
        <a className="sub" href={cusHref}>
          {value}
        </a>
      </div>
    );

  return (
    <div className="cus-card">
      <h4 className="title">{title}</h4>
      <span className="sub">{value}</span>
    </div>
  );
};

export default Title;
