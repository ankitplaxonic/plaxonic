import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const DataAnalyst = ({ setCurrentPage, location }) => {
  titleGenrator("Cloud Artificial Intelligence Services | Automation Solutions");
  setCurrentPage(location.pathname);

  return (
    <div className="data-analyst">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.dataAnalystImg.background} alt="" />
        <div className="title-block">
          <Title frontText="Data Analytics" backText="Services" backColor="rgb(251, 241, 239)" />
          <div className="para-block">
            <Paragraph>
              Every business that dreams to excel and turn digital needs accurate & precise data. Putting it in simple words, data is that valuable
              asset that allows businesses to explore the emerging opportunities, demanding customer expectations, possible challenges and the
              landscape, of course.
            </Paragraph>
            <Paragraph>
              Success of an organization to a great extent depends on the justified usage of statistics. In terms of growing expansion, success
              happens rapidly and feasibly. Through our data analytics consulting and big data analytics consulting, we fetch infinite success for our
              clients as per their expectations, goals and standards.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.dataAnalystImg.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.dataAnalystImg.contactImg} start="#eda7219e" end="#d65527" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(DataAnalyst));
