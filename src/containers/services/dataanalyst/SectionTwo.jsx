import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="What We Do" backText="Solutions" backColor="rgb(251, 241, 239)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.dataAnalystImg.secTwo} alt="" className="front-img" />
              <img src={images.dataAnalystImg.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Data and Analytics Planning" />
            <Paragraph>
              Our team represents company’s analytics initiatives to measure the possibilities that are bound to happen. We achieve this though
              data-driven approach.
            </Paragraph>
            <SubTitle title="Data Augmentation" />
            <Paragraph>
              By increasing assets of client’s business with the help of data and predictive analytics, we present all-round customer views and
              feedback.
            </Paragraph>
            <SubTitle title="Data Management" />
            <Paragraph>Apart from analytics and data synthesis, we also work on monetization, compliance and governance.</Paragraph>
            <SubTitle title="Data Democratization" />
            <Paragraph>
              To allow employees grasp and accept data to move forward in the direction of AI, we work on customizing user-friendly tools.
            </Paragraph>
            <SubTitle title="Industrialized solutions" />
            <Paragraph>
              We provide customized solutions for specific needs and turnkey solutions for challenges that show-up almost every day.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
