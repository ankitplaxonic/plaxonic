import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const Ai = ({ setCurrentPage, location }) => {
  titleGenrator("Cloud Artificial Intelligence Services | Automation Solutions");
  setCurrentPage(location.pathname);

  return (
    <div className="ai">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.aiImages.background} alt="" />
        <div className="title-block">
          <Title frontText="AI & Automation" backText="Services" backColor="rgb(245, 241, 255)" />
          <div className="para-block">
            <SubTitle title="Powering The Future With Technology!" />
            <Paragraph>
              Quite contrary to the common misbelief that Agile and DevOps are two entirely different ideas, they work best in synergy. Thanks to the
              deep insights of our team of experts, we know there is a lot more to Agile and DevOps, besides their connection with scrum and
              continuous delivery. It is too exciting to have them both working and crafting a work favorable atmosphere.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.aiImages.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.aiImages.contactImg} start="#7eb8ec" end="#125dae" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(Ai));
