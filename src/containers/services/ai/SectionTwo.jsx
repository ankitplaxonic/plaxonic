import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="Solutions We Provide" backText="Expertise" backColor="rgb(245, 241, 255)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.aiImages.secTwo} alt="" className="front-img" />
              <img src={images.aiImages.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Machine learning" />
            <Paragraph>
              We empower businesses as per the standards of machine learning technology in a way to interpret, manage complex data, figure out the
              trends and identify patterns.
            </Paragraph>
            <SubTitle title="Human language processing" />
            <Paragraph>
              Via this technology, we assure machines developed are good at understanding the human language and take the required actions minus any
              errors.
            </Paragraph>
            <SubTitle title="Digital virtual assistants" />
            <Paragraph>
              Virtual assistants we work on are pro at identifying human behavior as well as improve and support consumers experiences.
            </Paragraph>
            <SubTitle title="Business process automation" />
            <Paragraph>
              Robust applications created by us are smart enough to automatically conduct repetitive processes in terms of user based or machine
              learned instructions.
            </Paragraph>
            <SubTitle title="Decision management" />
            <Paragraph>
              We guide businesses to change and simplify business management solutions in accordance with artificial intelligence to drive results on
              the basis of algorithms and predictive systems.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
