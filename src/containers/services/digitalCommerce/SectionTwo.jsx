import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="Factors Leading To Sustainable Digital Commerce Solutions" backText="Criteria " backColor="rgb(240, 247, 253)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.digitalCommerceImg.secTwo} alt="" className="front-img" />
              <img src={images.digitalCommerceImg.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Omni-channel Commerce" />
            <Paragraph>
              As the name says, it’s all about being everywhere customers expects a business to be. Nothing else can help nurture healthy customer
              satisfaction rate.
            </Paragraph>
            <SubTitle title="Accelerated Customer Engagement" />
            <Paragraph>
              Thriving commerce platforms are embedded with tools that yield engaging experiences with customers and push businesses forward.
            </Paragraph>
            <SubTitle title="Better Conversion and Average Order Value" />
            <Paragraph>
              For the retailers, using standard platforms featured with accurate tools to handle potential customers is the way to go. We assist
              retailers to better their crucial KPIs.
            </Paragraph>
            <SubTitle title="Increased Agility" />
            <Paragraph>
              We integrate microservices architecture to support businesses in adapting to frequently changing market and customer demands via
              scalability and flexibility.
            </Paragraph>
            <SubTitle title="Expanded Categories" />
            <Paragraph>
              Marketplace integration is provided to businesses to easily add brands & products that are in demand and are current favorite of
              customers. This expands revenues and customer base.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
