import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const DigitalCommerce = ({ setCurrentPage, location }) => {
  titleGenrator("Digital Commerce Solutions | Ecommerce Multi Channel Solutions");
  setCurrentPage(location.pathname);

  return (
    <div className="digital-commerce">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.digitalCommerceImg.background} alt="" />
        <div className="title-block">
          <Title frontText="Digital Commerce" backText="Services" backColor="rgb(240, 247, 253)" />
          <div className="para-block">
            <Paragraph>
              Digital technologies of the modern world have transformed to 360-degree, thereby, changing customers approach and their way of
              communicating with businesses. Being demanding, they procure detailed information even when it’s the matter of single purchase.
            </Paragraph>
            <Paragraph>
              We cater to the needs of our clients with end-to-end digital commerce solutions that simplify the process of handling the complications
              of customer interaction during the times of buying and selling. With our expertise & knowledge, we personalize customer experiences that
              enhance their buying decisions not just in terms of speed but scale too.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.digitalCommerceImg.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.digitalCommerceImg.contactImg} start="#4677f6cf" end="#4a18cd" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(DigitalCommerce));
