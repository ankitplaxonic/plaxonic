import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const Cloud = ({ setCurrentPage, location }) => {
  titleGenrator("Blockchain Development Company | Experienced Blockchain Developers");
  setCurrentPage(location.pathname);

  return (
    <div className="cloud">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.cloudImg.background} alt="" />
        <div className="title-block">
          <Title frontText="Cloud" backText="Services" backColor="rgb(254, 247, 239)" />
          <div className="para-block">
            <Paragraph>
              We own a complete spectrum of digital and enterprise cloud services. While partnering with the prospects, we conduct an in-depth
              analysis of their objectives and development strategy. Implementing our technical knowledge and experience, we plan, execute and tackle
              client’s cloud estate for timely results that sync with future goals.
            </Paragraph>
            <Paragraph>
              We strongly believe that with the right approach and transformation strategy of cloud migration, achieving the digital evolution and
              empowering the business to surpass the contenders turns as easy as counting numbers.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.cloudImg.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.cloudImg.contactImg} start="#ffdc6f" end="#ecb119" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(Cloud));
