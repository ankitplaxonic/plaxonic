import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="Our Solutions For The Clients" backText="Expertise" backColor="rgb(254, 247, 239)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.cloudImg.secTwo} alt="" className="front-img" />
              <img src={images.cloudImg.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Cloud Advisory" />
            <Paragraph>
              Thanks to our comprehensive industry knowledge and expertise, we provide world-class vendor-neutral advisory assistance.
            </Paragraph>
            <SubTitle title="Cloud Migration" />
            <Paragraph>We migrate clients business to private, public as well as hybrid clouds along while bringing down risks.</Paragraph>
            <SubTitle title="Cloud Security" />
            <Paragraph>Protecting the cloud environment is crucial like anything to leverage dependable and progressive solutions.</Paragraph>
            <SubTitle title="Managed Cloud" />
            <Paragraph>By leveraging our managed services for public, private and hybrid clouds, clients can pull maximum value.</Paragraph>
            <SubTitle title="Private Cloud Onsite" />
            <Paragraph>
              Our private cloud services are created in a way to suit particular needs of the businesses at their desired location.
            </Paragraph>
            <SubTitle title="Public Cloud" />
            <Paragraph>We have an array of off-premise services for businesses trusting us for their cloud needs.</Paragraph>
            <SubTitle title="Private & Hybrid Cloud" />
            <Paragraph>
              To support digital business, we allow clients to get the best of our protected, compliant and agile private cloud infrastructure.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
