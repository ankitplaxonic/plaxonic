import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const DigitalProcess = ({ setCurrentPage, location }) => {
  titleGenrator("Cloud Artificial Intelligence Services | Automation Solutions");
  setCurrentPage(location.pathname);

  return (
    <div className="ai">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.digitalProcessImg.background} alt="" />
        <div className="title-block">
          <Title frontText="Digital Process Automation" backText="Services" backColor="rgba(236, 143, 82, 0.13)" />
          <div className="para-block">
            <Paragraph>
              In the light of highly dynamic business environment and wide digital transformation initiatives, digital process automation emerges out
              as the need of every progressive business. We partner with businesses to enrich them with enhanced business efficiency and prepare them
              to deliver world class customer service.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.digitalProcessImg.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.digitalProcessImg.contactImg} start="#e78e53" end="#b25a45" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(DigitalProcess));
