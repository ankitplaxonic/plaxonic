import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="Solutions We Provide" backText="Expansion" backColor="rgba(236, 143, 82, 0.13)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.digitalProcessImg.secTwo} alt="" className="front-img" />
              <img src={images.digitalProcessImg.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Organize Work From End To End" />
            <Paragraph>
              Our comprehensive industry knowledge concocted with extensive digitalization expertise is highly appreciated in the technology
              landscape. We believe in looping in the customers and employees in the process of automation.
            </Paragraph>
            <SubTitle title="Sustain Consistency Across Channels" />
            <Paragraph>
              We create unified customer journeys to imbibe loyalty and create cross-sell and up-selling opportunities regardless of which channel or
              device is at use. To make it possible, our highly determined team automates web, mobile and chat applications swiftly.
            </Paragraph>
            <SubTitle title="Augment Revenue And Profitability" />
            <Paragraph>
              Making our client’s organization future proof is one of our main objectives. We strive for taking revenue and profitability to the
              next-generation, omnichannel commercial platforms and digitize the complete supply chain.
            </Paragraph>
            <SubTitle title="Enjoy The Integrated Power Of BPM, RPA And AI " />
            <Paragraph>
              We power businesses to take combined leverage of BPM (Business Process Management), RPA (Robotic Process Automation) and AI (Artificial
              Intelligence). This results in transformation of complete business value chain.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
