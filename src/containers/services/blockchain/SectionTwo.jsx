import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="What We Offer" backText="Solutions" backColor="rgba(29, 181, 175, 0.08)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.blockchainImg.secTwo} alt="" className="front-img" />
              <img src={images.blockchainImg.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Consulting and Proof of Concept" />
            <Paragraph>
              We invest time and deploy our skills to understand objectives of the business and have a fair discussion with the client on multiple
              channels of blockchain implementation to form an idea and create a proof of concept.
            </Paragraph>
            <SubTitle title="B2B Enterprise Application" />
            <Paragraph>
              Via evolution of blockchain based B2B enterprise business application, organizations across the world are facilitated to associate in a
              defined and secure way, which of course, brings down possibilities of data tempering and fraudulent activities.
            </Paragraph>
            <SubTitle title="Custom Blockchain Networks" />
            <Paragraph>
              Right from designing to developing and handling custom decentralized blockchain networks, we do everything that’s needed to secure data
              retention and management.
            </Paragraph>
            <SubTitle title="Cloud Services" />
            <Paragraph>
              Thanks to our multiple cloud implementations, we integrate client’s blockchain application with both public as well as private cloud
              platforms.
            </Paragraph>
            <SubTitle title="Smart Contracts" />
            <Paragraph>
              We offer flawless coded smart contract development solutions to double-sure that our client’s blockchain is in sync with the accurate
              automation.
            </Paragraph>
            <SubTitle title="Development Services & eWallets" />
            <Paragraph>
              When we are approached for cryptocurrency development, cryptocurrency-based blockchain solutions, payment and exchange applications
              development and cryptocurrency wallet development, we make sure not to disappoint.
            </Paragraph>
            <SubTitle title="Blockchain Integration" />
            <Paragraph>
              For seamless migration with client’s software, our team creates distributed networks of ledgers, public and private blockchains and
              encryption software and merge them systematically.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
