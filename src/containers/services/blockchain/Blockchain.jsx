import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const Blockchain = ({ setCurrentPage, location }) => {
  titleGenrator("Blockchain Development Company | Experienced Blockchain Developers");
  setCurrentPage(location.pathname);

  return (
    <div className="block-chain">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.blockchainImg.background} alt="" />
        <div className="title-block">
          <Title frontText="Blockchain" backText="Services" backColor="rgba(29, 181, 175, 0.08)" />
          <div className="para-block">
            <Paragraph>
              Blockchain is for sure one of the most looked out revolutionary aspects for industries across the globe. Predicting rise in its
              importance even more in the coming time, we address blockchain along with its technologies like smartcontracts, distributed ledger and
              shared ledger.
            </Paragraph>
            <Paragraph>
              Our team puts forward its expertise in digital transformation as well as platform integration and merge them with blockchain to serve
              our clientele with value added services. Be it cyptographic algorithm or auditing the smart contracts, our genius minds do everything
              without a fail.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.blockchainImg.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.blockchainImg.contactImg} start="#02f9cad9" end="#19947d" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(Blockchain));
