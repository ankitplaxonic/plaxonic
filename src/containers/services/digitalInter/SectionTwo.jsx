import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="Contour Of Offerings" backText="Abilities " backColor="rgb(237, 247, 249)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.digitalCommerceImg.secTwo} alt="" className="front-img" />
              <img src={images.digitalCommerceImg.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Embracing The Digital Drift Intelligently" />
            <Paragraph>
              Businesses supposedly should be quick to embrace the changes in communication technology but at the same time it is a necessity to be
              flexible. We are best known for facilitating adoption of new channels by following a future-proof approach. Our clients should be open
              to incorporate new substitutes to SMS and email for A2P communication.
            </Paragraph>
            <SubTitle title="Taking On To The Proactive Routes" />
            <Paragraph>
              New operational model is synchronized in a way that call to actions come naturally to them. We make subtle use of contextual CTAs to act
              on the piece of information.
            </Paragraph>
            <SubTitle title="Delegation Of Authority To Customers" />
            <Paragraph>
              Our technical bots are determined to make each digital interaction more conducive for the customers. This is why our focus is on
              empowerment of customers with a striking automated self-service.
            </Paragraph>
            <SubTitle title="Not Eliminating Human Power" />
            <Paragraph>
              Abolishing human intervention completely from the communication channels would not be a good idea. There are several instances when
              customers want to speak with a human. We count upon the human potential for very tough and rare contingencies.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
