import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const DigitalInterAction = ({ setCurrentPage, location }) => {
  titleGenrator("Digital Commerce Solutions | Ecommerce Multi Channel Solutions");
  setCurrentPage(location.pathname);

  return (
    <div className="digital-interaction">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.digitalInterImg.background} alt="" />
        <div className="title-block">
          <Title frontText="Digital Interactions" backText="Services" backColor="rgb(237, 247, 249)" />
          <div className="para-block">
            <Paragraph>
              A business can reach to another level of technological uplift with enablement of making digital interactions. We know this and hence,
              partner with clients to bolster the best online interactions. We being quick at striking a delicate balance between the two
              contradicting forces- technology and humanness, make it work for our clients every single time.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.digitalInterImg.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.digitalInterImg.contactImg} start="#00b8cd" end="#095cb5" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(DigitalInterAction));
