import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const BusinessApplication = ({ setCurrentPage, location }) => {
  titleGenrator("Custom Software & Business Application Development Services");
  setCurrentPage(location.pathname);

  return (
    <div className="business-application">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.businessApplication.background} alt="" />
        <div className="title-block">
          <Title frontText="Business Applications" backText="Services" backColor="rgb(241, 249, 238)" />
          <div className="para-block">
            <Paragraph>
              To maintain pace with competitors and run the business smoothly & efficiently, businesses depend on a number of business applications.
              Growing popularity of these applications to support business processes of every type has clarified that IT services based on such
              platforms or apps are here to stay and are in every way, an important component of the wide IT services industry.
            </Paragraph>
            <Paragraph>
              Our experts let our clientele to use, integate and execute each & every business application we have in our panel. Our talented and
              professional consultants excel in offering consulation to assure return on technology investments of our partnered businesses grows at
              every step.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.businessApplication.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm start="#0eca51a8" end="#04a64e" />
      </Section>
    </div>
  );
};

const mapDispatchToProp = (dispatch) => ({
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});

export default connect(null, mapDispatchToProp)(withRouter(BusinessApplication));
