import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="Services Our Panel Constitutes Of" backText="Skills" backColor="rgb(241, 249, 238)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.businessApplication.secTwo} alt="" className="front-img" />
              <img src={images.businessApplication.secTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Consulting" />
            <Paragraph>
              To transform the businesses, we blend our leadership with unique technology and industry best practices to deliver standard and
              up-to-the mark solutions.
            </Paragraph>
            <SubTitle title="Analytics" />
            <Paragraph>
              Our analytics solution compromises of business performance and efforts to create appreciable business insights. It covers data
              visualisation & data discovery, data management, advanced analytics and big data.
            </Paragraph>
            <SubTitle title="Digital Engagement " />
            <Paragraph>
              Our extensive list of end-to-end services starts from foundation building and goes until development of digital strategy. Strategy
              assessment, business analysis, implementation and concept & design development, we do everything.
            </Paragraph>
            <SubTitle title="Business " />
            <Paragraph>
              By providing an appropriate ratio of best tools, proven services and required experience, we support businesses to incorporate multiple
              business processes in one enterprise-wide information system.
            </Paragraph>
            <SubTitle title="Technical " />
            <Paragraph>
              For quality & lasting technical services, we infuse revolutionary methodologies with industry best practices. This helps not only to
              manage life cycle of applications but also to custom create applications matching the standard of enterprises.
            </Paragraph>
            <SubTitle title="Application Products " />
            <Paragraph>
              We create applications that assist organizations and government agencies to enhance their partnerships and communication.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
