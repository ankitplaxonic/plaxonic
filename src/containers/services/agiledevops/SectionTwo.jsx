import React from "react";
import images from "../../../assets/images/images";
import Paragraph from "../../../components/paragraph/Paragraph";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";
import "./styles.scss";

const SectionTwo = () => {
  return (
    <div className="section-two">
      <Title frontText="The Practical Implications" backText="Solutions" backColor="rgb(254, 238, 239)" />
      <div className="sec-wrapper">
        <div className="row">
          <div className="col-md-6">
            <div className="img-block">
              <img src={images.agiledevops.agileTwo} alt="" className="front-img" />
              <img src={images.agiledevops.agileTwoBg} alt="" className="background" />
            </div>
          </div>
          <div className="col-md-6">
            <SubTitle title="Preparedness And Maturity Assessment" />
            <Paragraph>
              We understand that adopting Agile and DevOps is fundamentally important for organizations. Before exposing them to client’s system, we
              put in efforts to comprehend their company’s current status in agile adoption.
            </Paragraph>
            <SubTitle title="Agile And DevOps Inculcation Program" />
            <Paragraph>
              We conduct workshops and give lessons on business leadership training, development training, backlog preparation, Agile governance and
              Metrics, DevOps for Enterprise Agility and more.
            </Paragraph>
            <SubTitle title="Putting On Agile Mode" />
            <Paragraph>
              We pillar our potentials on assessing client’s current product engineering processes to identify gaps, providing customized training
              programs and rendering support for best possible execution. Furthermore, we adopt measures to ensure seamless Agile practices.
            </Paragraph>
            <SubTitle title="Access To Continuous Delivery" />
            <Paragraph>
              We dive in the pool of best agile and DevOps methodologies to undertake never ending route and enhance the product quality while slicing
              down the costs. For this, our people count on continuous integration, continuous delivery, test automation to name a few.
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionTwo;
