import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import images from "../../../assets/images/images";
import ContactForm from "../../../components/contact/ContactForm";
import Mainsection from "../../../components/mainsection/Mainsection";
import Paragraph from "../../../components/paragraph/Paragraph";
import Section from "../../../components/section/Section";
import SubTitle from "../../../components/title/SubTitle";
import Title from "../../../components/title/Title";
import constants from "../../../constants/constants";
import titleGenrator from "../../../title/title";
import SectionTwo from "./SectionTwo";
import "./styles.scss";

const Agiledevops = ({ setCurrentPage, location }) => {
  titleGenrator("Agile consulting services");

  setCurrentPage(location.pathname);

  return (
    <div className="agile-dev">
      <Mainsection customClass="service-page">
        <img className="main-bg" src={images.agiledevops.background} alt="" />
        <div className="title-block">
          <Title frontText="Agile And DevOps" backText="Services" backColor="rgb(254, 238, 239)" />
          <div className="para-block">
            <SubTitle title="Powering The Future With Technology!" />
            <Paragraph>
              Quite contrary to the common misbelief that Agile and DevOps are two entirely different ideas, they work best in synergy. Thanks to the
              deep insights of our team of experts, we know there is a lot more to Agile and DevOps, besides their connection with scrum and
              continuous delivery. It is too exciting to have them both working and crafting a work favorable atmosphere.
            </Paragraph>
          </div>
        </div>
        <div className="img-block">
          <img src={images.agiledevops.frontImg} alt="" />
        </div>
      </Mainsection>
      <Section>
        <SectionTwo />
      </Section>
      <Section customClass="contact-form">
        <ContactForm background={images.agiledevops.alignContactBg} start="#ff0c20cf" end="#c60119" />
      </Section>
    </div>
  );
};
const mapDispatchToProp = (dispatch) => ({
  handleMenuBg: (background) => dispatch({ type: constants.SET_MENU_BG, payload: background }),
  setCurrentPage: (pathname) =>
    dispatch({
      type: constants.SET_CURRENT_PAGE,
      payload: pathname,
    }),
});
export default connect(null, mapDispatchToProp)(withRouter(Agiledevops));
