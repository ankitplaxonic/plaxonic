import React from "react";
import { createPortal } from "react-dom";
import { connect } from "react-redux";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import constants from "../../../constants/constants";
import images from "../../../assets/images/images";

const Submenu = ({ subMenu, isSubOpen, currentSub, closeMenu, changeCurrentPage, currentPage }) => {
  if (!isSubOpen) return null;

  const subMenuList = currentSub.menu.map((menu) => {
    return (
      <li className="submenu-item" key={menu.id}>
        <Link to={menu.link} onClick={() => changeCurrentPage(menu.id)}>
          {menu.name}
        </Link>
      </li>
    );
  });
  // ${images.common.menuBg}
  const subMenuJsx = currentSub.menu && (
    <motion.div
      className="sub-menu"
      animate={!isSubOpen ? { scaleY: 0, scaleX: 0 } : { scaleY: 1, scaleX: 0.925 }}
      transition={{ duration: 0.3 }}
      style={{
        background: `${
          currentPage && (currentPage.background ? currentPage.background : "linear-gradient(to right, rgb(42, 49, 191), rgb(18, 129, 216))")
        }`,
      }}
    >
      <img src={images.common.menuLeftBg} alt="" className="menu-img-left" />
      <img src={images.common.menuRightBg} alt="" className="menu-img-right" />
      <div className="close-btn" onClick={() => closeMenu()}>
        <i className="fa fa-times"></i>
      </div>
      <div className="submenu-wrapper">
        <h3 className="menu-title">{currentSub.parent.subMenuTitle}</h3>
        <ul className="submenu-list">{subMenuList}</ul>
      </div>
    </motion.div>
  );

  return createPortal(subMenuJsx, document.getElementById("submenu"));
};

const mapStateToProp = (state) => ({
  subMenu: state.menu.menuItem,
  isSubOpen: state.menu.isSubOpen,
  currentSub: state.menu.currentSub,
  currentPage: state.menu.currentPage,
});

const mapDispatchToProp = (dispatch) => ({
  closeMenu: () =>
    dispatch({
      type: constants.MENU_CLOSE,
    }),
  changeCurrentPage: (id) =>
    dispatch({
      type: constants.CHANGE_CURRENT_PAGE,
      payload: id,
    }),
});

export default connect(mapStateToProp, mapDispatchToProp)(Submenu);
