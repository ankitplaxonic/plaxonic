import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import constants from "../../../constants/constants";

import "../styles.scss";

const Navlist = ({ menuItem, OpenSubMenu }) => {
  const handleMenuClick = () => {
    window.scrollTo(0, 0);
  };

  const navlink =
    menuItem &&
    menuItem.map((nav) => (
      <div className="nav-link-wrapper" key={nav.id}>
        {nav.hasDropDown ? (
          <div
            className="nav-link nav-link-span"
            data-name={nav.name}
            onClick={
              nav.hasDropDown
                ? () => {
                    OpenSubMenu(nav.id);
                    handleMenuClick();
                  }
                : null
            }
          >
            <span className="name">{nav.name}</span>
            {nav.hasDropDown ? <i className="fa fa-caret-down" aria-hidden="true"></i> : null}
          </div>
        ) : (
          <NavLink className="nav-link" onClick={() => handleMenuClick()} data-name={nav.name} to={nav.isDropDown ? "/" : nav.link}>
            <span className="name">{nav.name}</span>
          </NavLink>
        )}
      </div>
    ));

  return <div className="nav-wrapper">{navlink}</div>;
};

const mapStateToProp = (state) => ({ menuItem: state.menu.menuItem });

const mapDispatchToProp = (dispatch) => ({
  OpenSubMenu: (id) =>
    dispatch({
      type: constants.OPEN_MENU,
      payload: { id },
    }),
});

export default connect(mapStateToProp, mapDispatchToProp)(Navlist);
