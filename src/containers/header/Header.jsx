import React, { useState } from "react";
import { connect } from "react-redux";
import images from "../../assets/images/images";
import constants from "../../constants/constants";
import Navlist from "./navbar/Navlist";
import "./styles.scss";
import Submenu from "./submenu/Submenu";
import { Link, withRouter } from "react-router-dom";

const Header = ({ location }) => {
  const [count, setCount] = useState(false);

  // console.log(closeMenu)
  window.addEventListener("scroll", function () {
    if (window.scrollY >= 150 && window.scrollY < 300 && count) {
      // closeMenu();
      setCount(false);
    } else {
      setCount(true);
    }
  });

  return (
    <header>
      <div className="menuwrapper">
        <Submenu />
        <div className="left">
          <div className="logo">
            <Link to="/">
              {location.pathname === "/" ? <img src={images.common.logoWhite} alt="" /> : <img src={images.common.logoBlack} alt="" />}
            </Link>
          </div>
        </div>
        <div className="right">
          <Navlist />
        </div>
      </div>
    </header>
  );
};

const mapStateToProps = (state) => ({
  currentPage: state.menu.currentPage,
});

const mapDispatchToProp = (dispatch) => ({
  // closeMenu: () =>
  //   dispatch({
  //     type: constants.MENU_CLOSE,
  //   }),
});

export default connect(mapStateToProps, mapDispatchToProp)(withRouter(Header));
