const constants = {
  OPEN_MENU: "open_menu",
  MENU_CLOSE: "menu_close",
  CHANGE_CURRENT_PAGE: "chnage_current_page",
  SET_DEFAULT_MENU_BG: "set_default_menu_bg",
  SET_MENU_BG: "set_menu_bg",
  SET_CURRENT_PAGE: "set_current_page",
  SET_NOT_FOUND_MENU_BG: "set_not_found_menu_bg",
};

export default constants;
