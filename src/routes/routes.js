import Home from "../containers/home/Home";
import NotFound from "../containers/notfound/NotFound";
import TechStack from "../containers/techstack/TechStack";
import Agiledevops from "../containers/services/agiledevops/Agiledevops";
import Ai from "../containers/services/ai/Ai";
import BusinessApplication from "../containers/services/businessapplication/BusinessApplication";
import Blockchain from "../containers/services/blockchain/Blockchain";
import Cloud from "../containers/services/cloud/Cloud";
import DataAnalyst from "../containers/services/dataanalyst/DataAnalyst";
import DigitalCommerce from "../containers/services/digitalCommerce/DigitalCommerce";
import DigitalInterAction from "../containers/services/digitalInter//DigitalInterAction";
import DigitalProcess from "../containers/services/digital-process/DigitalProcess";
import Leadership from "../containers/about-us/leadership/Leadership";

const routes = [
  { id: 0, exact: true, path: "/", component: Home },
  { id: 1, exact: true, path: "/tech-stack", component: TechStack },
  { id: 2, exact: true, path: "/services/agile-and-devops", component: Agiledevops },
  { id: 3, exact: true, path: "/services/ai-automation", component: Ai },
  { id: 4, exact: true, path: "/services/business-applications", component: BusinessApplication },
  { id: 5, exact: true, path: "/services/blockchain", component: Blockchain },
  { id: 6, exact: true, path: "/services/cloud", component: Cloud },
  { id: 7, exact: true, path: "/services/data-analytics", component: DataAnalyst },
  { id: 8, exact: true, path: "/services/digital-commerce", component: DigitalCommerce },
  { id: 9, exact: true, path: "/services/digital-interactions", component: DigitalInterAction },
  { id: 10, exact: true, path: "/services/digital-process-automation", component: DigitalProcess },
  { id: 11, exact: true, path: "/about-us/leadership", component: Leadership },
  { id: 8, exact: true, path: "*/not-found", component: NotFound },
];

export default routes;
