import background from "./process-1.png";
import frontImg from "./process-2.png";
import secTwo from "./process-3.png";
import secTwoBg from "./process-4.png";

const digitalProcessImg = { background, frontImg, secTwo, secTwoBg };

export default digitalProcessImg;
