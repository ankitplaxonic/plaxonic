import background from "./business-header.png";
import frontImg from "./business.png";
import secTwo from "./business2.png";
import secTwoBg from "./business2-bg.png";

const businessApplication = {
  background,
  frontImg,
  secTwo,
  secTwoBg,
};

export default businessApplication;
