import background from "./background.png";
import frontImg from "./front-img.png";
import agileTwo from "./agile2.png";
import agileTwoBg from "./agile2-bg.png";
import alignContactBg from "./agile-contact.png";

const agiledevops = { background, frontImg, agileTwo, agileTwoBg, alignContactBg };

export default agiledevops;
