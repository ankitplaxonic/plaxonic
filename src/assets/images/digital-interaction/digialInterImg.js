import background from "./service_img1.png";
import frontImg from "./service_img2.png";
import secTwo from "./service_img3.png";
import secTwoBg from "./service_img4.png";

const digitalInterImg = { background, frontImg, secTwo, secTwoBg };

export default digitalInterImg;
