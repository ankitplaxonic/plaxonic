import background from "./analytic-header.png";
import frontImg from "./analytic.png";
import secTwo from "./analytic2.png";
import secTwoBg from "./analytic2-bg.png";

const dataAnalystImg = { background, frontImg, secTwo, secTwoBg };

export default dataAnalystImg;
