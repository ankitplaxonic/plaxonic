import background from "./asset 19.png";
import floatingOne from "./floating-1.png";
import floatingTwo from "./floating-2.png";
import floatingThree from "./floating-3.png";
import frontImg from "./Shap2.png";

const notFound = {
  background,
  floatingOne,
  floatingTwo,
  floatingThree,
  frontImg,
};

export default notFound;
