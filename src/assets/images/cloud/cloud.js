import background from "./cloud-header.png";
import frontImg from "./cloud.png";
import secTwo from "./cloud2.png";
import secTwoBg from "./cloud2-bg.png";

const cloudImg = { background, frontImg, secTwo, secTwoBg };

export default cloudImg;
