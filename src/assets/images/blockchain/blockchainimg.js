import background from "./blockchain-bg-1.png";
import frontImg from "./blockchain-1.png";
import secTwo from "./blockchain-2.png";
import secTwoBg from "./blockchain-bg-2.png";

const blockchainimg = { background, frontImg, secTwo, secTwoBg };

export default blockchainimg;
