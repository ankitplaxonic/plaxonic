import logoWhite from "./common/logo-white.svg";
import logoBlack from "./common/logo-black.svg";
import banneOneBg from "./home/banner-1.jpg";
import banneTwoBg from "./home/banner-2.jpg";
import banneThreeBg from "./home/banner-3.jpg";
import banneFourBg from "./home/banner-4.jpg";
import menuLeftBg from "./common/menu-left.png";
import menuRightBg from "./common/menu-right.png";
import menuBg from "./common/menu-bg.jpg";
import bannerBtm from "./home/banner-strip.svg";
import videoWrapper from "./home/video-wrapper.png";
import ai from "./home/ai-img.png";
import ba from "./home/business-application.png";
import blockChain from "./home/blockchain-img.png";
import secTwoBg from "./home/sec-two-bg.svg";
import secTwoFg from "./home/sec-two-fg.svg";
import secThreeBg from "./home/third-section-bg2.png";
import secThreeImg from "./home/cop_res.png";
import indiaThumb from "./home/india-thumb.jpg";
import indiaMain from "./home/india-main.png";
import usaThumb from "./home/usa-thumb.jpg";
import usaMain from "./home/usa-main.png";
import dubaiThumb from "./home/dubai-thumb.jpg";
import dubaiMain from "./home/dubai-main.png";
import hongKongThumb from "./home/hong-kong-thumb.jpg";
import hongKongMain from "./home/hong-kong-main.png";
import homeFlyingOne from "./home/move3.png";
import contactImg from "./common/contact-person.png";
import contactBg from "./common/contact-bg.png";
import bannerFgTwo from "./home/banner-2nd-img.png";
import bannerFgThree from "./home/banner-3rd-img.png";
import bannerFgFour from "./home/banner-4th-image.png";
import goodFirmBadge from "./common/view-profile.svg";
import backgroundImg from "./common/map-footer.png";
import mainBg from "./techstack/bg-img.png";
import techLaptop from "./techstack/laptop.png";
import techImg from "./techstack/tech-stack";
import notFound from "./notfound/not-found";
import agiledevops from "./agiledevops/agiledevops";
import aiImages from "./ai/ai";
import businessApplication from "./business-application/businessApplication";
import blockchainImg from "./blockchain/blockchainimg";
import cloudImg from "./cloud/cloud";
import dataAnalystImg from "./data-analyst/dataAnalystImg";
import digitalCommerceImg from "./digital-commerce/digitalCommerce";
import digitalInterImg from "./digital-interaction/digialInterImg";
import digitalProcessImg from "./digital-process/digitalProcessImg";
import leaderShipImg from "./leadership/leadershipImg";

const images = {
  common: {
    logoWhite,
    logoBlack,
    menuBg,
    contactImg,
    contactBg,
    menuLeftBg,
    menuRightBg,
  },
  home: {
    bannerBtm,
    videoWrapper,
    secTwoBg,
    secTwoFg,
    secThreeBg,
    secThreeImg,
    sectionTwo: [
      { id: 1, img: ai },
      { id: 2, img: ba },
      { id: 3, img: blockChain },
    ],
    banner: [
      { id: 1, bgImage: banneOneBg, fgImage: null },
      { id: 2, bgImage: banneTwoBg, fgImage: bannerFgTwo },
      { id: 3, bgImage: banneThreeBg, fgImage: bannerFgThree },
      { id: 4, bgImage: banneFourBg, fgImage: bannerFgFour },
    ],
    secFour: [
      { id: 1, thumbnail: indiaThumb, main: indiaMain },
      { id: 2, thumbnail: usaThumb, main: usaMain },
      { id: 3, thumbnail: dubaiThumb, main: dubaiMain },
      { id: 4, thumbnail: hongKongThumb, main: hongKongMain },
    ],
    homeFlyingOne,
  },
  techstack: { mainBg, techLaptop, card: { ...techImg } },
  footer: { goodFirmBadge, logoBlack, backgroundImg },
  notFound,
  agiledevops,
  aiImages,
  businessApplication,
  blockchainImg,
  cloudImg,
  dataAnalystImg,
  digitalCommerceImg,
  digitalInterImg,
  digitalProcessImg,
  leaderShipImg,
};

export default images;
