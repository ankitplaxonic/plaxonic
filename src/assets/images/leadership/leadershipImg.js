import bannerImg from "./banner.png";
import shaan from "./shaan.png";
import shaanBG from "./shaan-bg.png";
import shaanModal from "./shaan-modal.jpg";
import kareem from "./kareem.png";
import kareemBG from "./kareem-bg.png";
import kareemModal from "./kareem-modal.jpg";
import nitish from "./nitish.png";
import nitishBG from "./nitish-bg.png";
import nitishModal from "./nitish-modal.jpg";
import sunny from "./sunny.png";
import sunnyBG from "./sunny-bg.png";
import sunnyModal from "./sunny-modal.jpg";
import mohit from "./mohit.png";
import mohitBG from "./mohit-bg.png";
import mohitModal from "./mohit-modal.jpg";
import ajay from "./ajay.png";
import ajayBG from "./ajay-bg.png";
import ajayModal from "./ajay-modal.jpg";
import dipti from "./dipti.png";
import diptiBG from "./dipti-bg.png";
import diptiModal from "./dipti-modal.jpg";
import gagan from "./gagan.png";
import gaganBG from "./gagan-bg.png";
import gaganModal from "./gagan-modal.jpg";
import nadeem from "./nadeem.png";
import nadeemBG from "./nadeem-bg.png";
import nadeemModal from "./nadeem-modal.jpg";
import saurabh from "./saurabh.png";
import saurabhBG from "./saurabh-bg.png";
import saurabhModal from "./saurabh-modal.jpg";

const leaderShipImg = {
  bannerImg,
  mainImg: [
    { id: 0, img: shaan, bgImg: shaanBG, modalImg: shaanModal },
    { id: 1, img: kareem, bgImg: kareemBG, modalImg: kareemModal },
    { id: 2, img: nitish, bgImg: nitishBG, modalImg: nitishModal },
    { id: 3, img: sunny, bgImg: sunnyBG, modalImg: sunnyModal },
    { id: 4, img: mohit, bgImg: mohitBG, modalImg: mohitModal },
    { id: 5, img: ajay, bgImg: ajayBG, modalImg: ajayModal },
    { id: 6, img: dipti, bgImg: diptiBG, modalImg: diptiModal },
    { id: 7, img: gagan, bgImg: gaganBG, modalImg: gaganModal },
    { id: 8, img: nadeem, bgImg: nadeemBG, modalImg: nadeemModal },
    { id: 9, img: saurabh, bgImg: saurabhBG, modalImg: saurabhModal },
  ],
};
export default leaderShipImg;
