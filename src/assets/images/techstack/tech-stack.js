import reactJs from "./frontend/front-1.svg";
import ionic from "./frontend/front-2.svg";
import angularJs from "./frontend/front-3.svg";
import vueJs from "./frontend/front-4.svg";
import iosNative from "./mobile/mobile-1.svg";
import androidNative from "./mobile/mobile-2.svg";
import nodeJs from "./backend/back-1.svg";
import dotNet from "./backend/back-2.svg";
import java from "./backend/back-3.svg";
import spring from "./backend/back-4.svg";
import hibernate from "./backend/back-5.svg";
import goLang from "./backend/back-6.svg";
import python from "./backend/back-7.svg";
import mySql from "./mysql.svg";
import sqlServer from "./microsoft-sql-server.svg";
import mongoDb from "./mongodb.svg";
import couchDb from "./couch db.svg";
import dynamoDb from "./aws-dynamodb.svg";
import postgreSQL from "./postgresql.svg";
import redis from "./redis.svg";
import memcached from "./memcached.svg";
import berkelyDB from "./oracle berkeley DB .svg";
import terraStore from "./terrastore.svg";
import scalaris from "./scalaris.svg";
import bigTable from "./bigtable.svg";
import neoForJ from "./neo4j.svg";
import cassandra from "./cassandra.svg";
import voldemort from "./voldemort.svg";
import simpleDb from "./aws-2.svg";
import googleCloud from "./infrastructure/infra-1.svg";
import aws from "./infrastructure/infra-2.svg";
import digitalOcean from "./infrastructure/infra-3.svg";
import azure from "./infrastructure/infra-4.svg";
import sonarQube from "./sonarqube.svg";
import jenkins from "./jenkins-1.svg";
import git from "./git-icon.svg";
import nexus from "./nexus.svg";
import go from "./go.svg";
import apache from "./apache maven2.svg";
import jMeter from "./jmeter.svg";
import selenium from "./selenium-logo.svg";
import cucumber from "./cucumber.svg";
import appium from "./appium.svg";
import snort from "./snort-icon.svg";
import kubernetes from "./kubernets.svg";
import docker from "./docker.svg";
import chef from "./chef-13.svg";
import puppet from "./puppet-1.svg";
import terraform from "./terraform-enterprise.svg";
import elasticSearch from "./elasticsearch.svg";
import logStash from "./elastic-logstash.svg";

const techImg = {
  frontEndWeb: [
    { id: 1, img: reactJs, name: "React Js" },
    { id: 2, img: ionic, name: "Ionic" },
    { id: 3, img: angularJs, name: "Angular js" },
    { id: 4, img: vueJs, name: "Vue Js" },
  ],
  frontEndMobile: [
    { id: 1, img: iosNative, name: "Ios Native" },
    { id: 2, img: androidNative, name: "Android Native" },
    { id: 3, img: reactJs, name: "React Native" },
    { id: 4, img: ionic, name: "Iconic" },
  ],
  backEnd: [
    { id: 1, img: nodeJs, name: "NodeJs" },
    { id: 2, img: dotNet, name: ".Net" },
    { id: 3, img: java, name: "Java" },
    { id: 4, img: spring, name: "Spring" },
    { id: 5, img: hibernate, name: "Hibernate" },
    { id: 6, img: goLang, name: "GoLang" },
    { id: 7, img: python, name: "Python" },
  ],
  persist: [
    { id: 1, img: mySql, name: "mySQL" },
    { id: 2, img: sqlServer, name: "SQL Server" },
    { id: 3, img: mongoDb, name: "MongoDB" },
    { id: 4, img: couchDb, name: "CouchDB" },
    { id: 5, img: dynamoDb, name: "DynamoDB" },
    { id: 6, img: postgreSQL, name: "PostgreSQL" },
    { id: 7, img: redis, name: "Redis" },
    { id: 8, img: memcached, name: "Memcached" },
    { id: 9, img: berkelyDB, name: "Berkely DB" },
    { id: 10, img: terraStore, name: "Terra Store" },
    { id: 11, img: scalaris, name: "Scalaris" },
    { id: 12, img: bigTable, name: "Big Table" },
    { id: 13, img: neoForJ, name: "Neo4j" },
    { id: 14, img: cassandra, name: "Cassandra" },
    { id: 15, img: voldemort, name: "Voldemort" },
    { id: 16, img: simpleDb, name: "SimpleDB" },
  ],
  Infrastructure: [
    { id: 1, img: googleCloud, name: "Google Cloud" },
    { id: 2, img: aws, name: "AWS" },
    { id: 3, img: digitalOcean, name: "Digital Ocean" },
    { id: 4, img: azure, name: "Azure" },
  ],
  cicd: [
    { id: 1, img: sonarQube, name: "Sonar Qube" },
    { id: 2, img: jenkins, name: "Jenkins" },
    { id: 3, img: git, name: "Git" },
    { id: 4, img: nexus, name: "Nexus" },
    { id: 5, img: go, name: "Go" },
    { id: 6, img: apache, name: "Apache Maven" },
  ],
  testing: [
    { id: 1, img: jMeter, name: "JMeter" },
    { id: 2, img: selenium, name: "Selenium" },
    { id: 3, img: cucumber, name: "Cucumber" },
    { id: 4, img: appium, name: "Appium" },
    { id: 5, img: snort, name: "Snort" },
  ],
  infrastructureAutomation: [
    { id: 1, img: kubernetes, name: "Kubernetes" },
    { id: 2, img: docker, name: "Docker" },
    { id: 3, img: chef, name: "Chef" },
    { id: 4, img: puppet, name: "Puppet" },
    { id: 5, img: terraform, name: "Terraform" },
  ],
  monitoring: [
    { id: 1, img: elasticSearch, name: "Elastic Search" },
    { id: 2, img: logStash, name: "Log Stash" },
  ],
};

export default techImg;
