import background from "./commerce-header.png";
import frontImg from "./commerce.png";
import secTwo from "./commerce2.png";
import secTwoBg from "./commerce2-bg.png";

const digitalCommerceImg = { background, frontImg, secTwo, secTwoBg };

export default digitalCommerceImg;
