import images from "../../images/images";

const details = [
  {
    id: 0,
    firstName: "Shaan",
    lastName: "Rizvi",
    position: "Founder & CEO",
    bio: `Shaan Rizvi, the Founder and CEO of Plaxonic Technologies, is a young and empathetic entrepreneur who chronicles the best ways to innovate and thrive. Previously he worked as a telesales executive in a small BPO in Delhi, as he was inclined towards sales and customer service. In 2013, he founded Plaxonic Technologies with a dream of having a company with happy employees. He has given shape to his dream during all these past succesful years. Now there are more than 100 happy people. He is fun loving, hard-working, and meticulous in nature. In his free time, Shaan nurtures ideas towards the happiness of his employees. He loves riding his bike and exploring nature.`,
    color: "#306bed",
  },
  {
    id: 1,
    firstName: "Kareem",
    lastName: "El. Deeb",
    position: "COO MENA",
    bio: `A Dynamic Executive (Bilingual - English / Arabic) with over 10 years experience
    in developing and implementing global marketing, communications and business
    development strategies across business to business (B2B), Kareem El.Deeb is
    based in Dubai. Along with 8+ Years of Experience in the field of Web Design &
    Development, he has 15+ years of Combined Experience in the field of Creative
    Design / Art Direction. Mr. El.Deeb’s leadership qualities not only motivate
    people but guide them to challenge their skills and enhance them further. From
    Vodafone, Bandridge, Jotun paints, Golds Gym to Compsec he has worked with
    world’s most renowned brands. His strong personality & optimistic thinking are
    two powerful attributes that help him set benchmarks, excel in them and create
    an unparalleled performance-based marketing strategy. `,
    color: "rgb(234, 164, 26)",
  },
  {
    id: 2,
    firstName: "Nitish",
    lastName: "Chauhan",
    position: "Operation Head-Sales",
    bio: `Nitish Chauhan is holding the position of Head-Outbound Sales in Plaxonic
    Technologies. He is a graduate of Santosh University, where he received a
    Bachelor’s degree in Occupational Therapy. 
    He was least interested in medical profession and hence started his career as a
    sales executive in the year 2007. After having 6 years of immense experience,
    Nitish joined Plaxonic in the year 2013. He did a commendable job in the company
    and was awarded as the ‘employee of the year’ and ‘best sales of the year’ in
    2014 and 2015 respectively.
    A die-hard fitness freak, he makes sure not to miss his workout session ever. In
    his free time, he loves to play chess and cricket with his friends. Despite
    having a great personality, he always remains down-to-earth. 
    `,
    color: "rgb(138, 94, 233)",
  },
  {
    id: 3,
    firstName: "Sunny",
    lastName: "Khanna",
    position: "Operation Head-Non-Sales",
    bio: `With the caliber of organizing and prioritizing both the clients and in-house
    projects, Sunny Khanna turns everyday challenges into fruitful outcomes. He has
    7+ years of experience in leading onshore team and deliver excellent software
    solutions. Attention to details, attitude of instilling confidence in the
    employees and motivating them towards reaching their goals & expectations
    without violating professional decorum, count for his strong points. 
    With specialization in development, digital transformation, conceptualizing
    business process strategies and handling data science visualization concept, he
    works across the non-sales team to bring a solid and all-round development
    skill-set.
    Sunny holds degree in BCA from Guru Nanak Dev University and M.B.A (IT) from
    Madurai Kamraj University. Besides his dedication towards work, he is a
    passionate rider. He loves traveling to varied places on his bike. In his
    pastime, he loves exploring new things on the internet.
    `,
    color: "#306bed",
  },
  {
    id: 4,
    firstName: "Mohit",
    lastName: "Sharma",
    position: "Head Lead Generation",
    bio: `Mohit Sharma started his career as a customer support representative with a BPO
    in July 2006. Now he holds the position of Head - Lead Generation with Plaxonic
    Technologies. With lots of struggle, immense knowledge and years of experience,
    he has succeeded in his goals. His passion and dedication towards work has
    helped him achieve the leadership award in 2015. He is goal oriented, modest and
    hardworking and has been always recognized for his deeds and achievements. 
    Mohit has completed his graduation from Meerut University and MBA from Amity
    University. He has struggled a lot in his life; however, he never lost hope. In
    his spare time, he loves being with his wife and 2 sweet daughters.
    `,
    color: "rgb(80, 89, 168)",
  },
  {
    id: 5,
    firstName: "Ajay",
    lastName: "Pandey",
    position: "Head Inbound Sale",
    bio: `Ajay Pandey is associated with Plaxonic Technologies from the foundation days of
    the company. Now he holds the position of Head- Inbound Sales. He is known to be
    an aggressive leader in terms of pushing his team towards targets. At the same
    time, he empathizes with colleagues to relieve them from stress. He has
    go-getter attitude, and enjoys overcoming obstacles. His amazing management
    skills have helped him achieve the ‘best Leader award for the year 2015-2016’.
    Ajay is a humanities graduate from Delhi University and was always there in good
    books of his teachers. He takes inspiration and sportsman spirit from the sports
    he plays such as Cricket, Hockey and other outdoor games. He is a fitness freak
    and loves spending time in gym.
    `,
    color: "rgb(34, 191, 120)",
  },
  {
    id: 6,
    firstName: "Dipti",
    lastName: "Sharma",
    position: "Manager Human Resource",
    bio: `Dipti Sharma started her career as a customer support representative with a BPO
    in July 2006. Now she holds the position of Manager -Human Resource with
    Plaxonic Technologies. With lots of struggle, immense knowledge, and years of
    experience, she has succeeded in her goals. Her passion and dedication towards
    work has helped her achieve the ‘Employee of the Year’ award in 2016. She is
    goal oriented, modest and hardworking and has been always recognized for her
    deeds and achievements.
    Dipti has completed her graduation from Delhi University and MBA from SMU. In her
    spare time, she loves spending time with kids.
    `,
    color: "rgb(14, 190, 231)",
  },
  {
    id: 7,
    firstName: "Gagan",
    lastName: "Mishra",
    position: "Manager Accounts and Finance ",
    bio: `Gagan Mishra being the Manager- Accounts and Finance takes on a vast array of
    roles in the organization. His main strengths include his eye for detail,
    precision, and accuracy while dealing in numbers. He started his career as an
    account executive and today his dedication and hard work has helped him reach
    high level. From his starting days till now he has grown with the company. He
    understands the nature of his job very well and takes care of the
    confidentiality in the best interest of company.
    He graduated in Commerce from LNM University with flying colors. He has an
    understanding nature and follows business ethics strictly. Listening music and
    playing cricket is something what he cherishes to do in his leisure time.
    `,
    color: "rgb(232, 129, 71)",
  },
  {
    id: 8,
    firstName: "Nadeem",
    lastName: "Khan",
    position: "Head Admin & IT",
    bio: `As the head of Admin & IT, Nadeem Khan has always been a dedicated employee. He
    is passionate about technology, which has always helped him excel in his field
    and reach heights. He holds a degree in MCSE. After completing his education,
    Nadeem started looking up for a job and landed in Plaxonic Technologies.
    He was born in a small village and hence struggled a lot in life to achieve a
    milestone. His journey from an executive to an Admin & IT Head was hard,
    however, he has always enjoyed the ups and downs.In his spare time, Nadeem loves to hum old melodious songs. He has performed in
    many company events and has touched the soul of everyone with his beautiful
    voice.
    `,
    color: "rgb(183, 104, 196)",
  },
  {
    id: 9,
    firstName: "Saurabh",
    lastName: "Singh",
    position: "Head Technical Support",
    bio: `Saurabh Singh started his career as a business development executive and is
    currently designated as Head Technical support. Being a young and dynamic
    person, he has soon adapted with the challenges of BPO process. He is
    successfully heading a team of expert technicians. It will be not wrong to say
    that Saurabh molded himself with adversities. This is very evident from his
    journey within the company as he kept on climbing the ladder of success.
    Adventurous by nature, Saurabh finds life in traveling. Saurabh is an engineering
    graduate. He has completed his B.tech with flying colors from Greater Noida
    Institute of Technology (Greater Noida). He is CCNA certified and has completed
    training in embedded systems.
    `,
    color: "rgb(175, 140, 100)",
  },
];

const data = details.map((item, index) => {
  return item.id === images.leaderShipImg.mainImg[index].id ? { ...item, ...images.leaderShipImg.mainImg[index] } : null;
});

export default data;
