import constants from "../constants/constants";

const initialState = {
  menuItem: [
    {
      id: 1,
      name: "Services",
      attr: "services",
      link: "/",
      hasDropDown: true,
      subMenuTitle: "Services",
      dropDown: [
        {
          id: 1,
          name: "Agile And DevOps",
          link: "/services/agile-and-devops",
          background: "linear-gradient(to right, rgb(213, 0, 22), rgba(255, 11, 32, 0.89))",
        },
        {
          id: 2,
          name: "AI & Automation",
          link: "/services/ai-automation",
          background: "linear-gradient(to right, rgba(2, 175, 248, 0.71), rgb(0, 86, 148))",
        },
        {
          id: 3,
          name: "Business Applications",
          link: "/services/business-applications",
          background: "linear-gradient(to right, rgb(18, 131, 52), rgb(21, 211, 77))",
        },
        { id: 4, name: "Blockchain", link: "/services/blockchain", background: "linear-gradient(to right, rgb(10, 127, 118), rgb(11, 215, 193))" },
        { id: 5, name: "Cloud", link: "/services/cloud", background: "linear-gradient(to right, rgb(189, 138, 0), rgb(255, 203, 41))" },
        {
          id: 6,
          name: "Data Analytics",
          link: "/services/data-analytics",
          background: "linear-gradient(to right, rgb(208, 107, 35), rgb(238, 169, 34))",
        },
        {
          id: 7,
          name: "Digital Commerce",
          link: "/services/digital-commerce",
          background: "linear-gradient(to right, rgb(76, 66, 243), rgba(82, 97, 246, 0.7411764705882353))",
        },
        {
          id: 8,
          name: "Digital Interactions",
          link: "/services/digital-interactions",
          background: "linear-gradient(to right, rgb(0, 115, 198), rgb(0, 199, 204))",
        },
        {
          id: 9,
          name: "Digital Process Automation",
          link: "/services/digital-process-automation",
          background: "linear-gradient(to right, rgb(130, 50, 64), rgb(236, 143, 83))",
        },
        { id: 10, name: "Digital Transcription", link: "/services/digital-transcription" },
        { id: 11, name: "Digital Transformation", link: "/services/digital-transformation" },
        { id: 12, name: "Mobile App", link: "/services/mobile-app" },
        { id: 13, name: "Omnichannel", link: "/services/omnichannel" },
        { id: 14, name: "Open Source Development", link: "/services/open-source-development" },
        { id: 15, name: "SPA", link: "/services/spa" },
        { id: 16, name: "Website Design & Development", link: "/services/website-esign-development" },
      ],
    },
    { id: 2, name: "Tech Stack", attr: "tech-stack", link: "/tech-stack", hasDropDown: false },
    {
      id: 3,
      name: "About Us",
      attr: "about-us",
      link: "/#",
      hasDropDown: true,
      subMenuTitle: "About Us",
      dropDown: [
        { id: 1, name: "Overview", link: "/overview" },
        { id: 2, name: "Leadership", link: "/about-us/leadership" },
        { id: 3, name: "Why Plaxonic Technologies?", link: "/why-plaxonic" },
        { id: 4, name: "Community", link: "/community" },
        { id: 5, name: "Culture", link: "/culture" },
      ],
    },
    {
      id: 4,
      name: "Career",
      attr: "career",
      link: "/#",
      hasDropDown: true,
      subMenuTitle: "Career",
      dropDown: [
        { id: 1, name: "Why Work With Plaxonic?", link: "/why-work-with-plaxonic" },
        { id: 2, name: "Current Openings", link: "/current-openings" },
        { id: 3, name: "Internship (Training) & Hiring Programs", link: "/internship" },
        { id: 4, name: "Fraudulent Policy", link: "/fraudulent-policy" },
      ],
    },
    { id: 5, name: "contact-us", link: "/#", hasDropDown: false },
  ],
  isSubOpen: false,
  currentSub: { menu: null, parent: null },
  currentPage: { background: "linear-gradient(to right, rgb(42, 49, 191), rgb(18, 129, 216))" },
};

export default function menu(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case constants.OPEN_MENU: {
      const selectedMenu = state.menuItem.find((menu) => menu.id === payload.id);

      return { ...state, isSubOpen: true, currentSub: { menu: selectedMenu.dropDown, parent: selectedMenu } };
    }
    case constants.MENU_CLOSE:
      // return state;
      console.log({ ...state });
      return {
        ...state,
        isSubOpen: false,
        currentSub: { ...state.currentSub, menu: null, parent: null },
      };
    case constants.CHANGE_CURRENT_PAGE:
      const page = state.currentSub.parent.dropDown.find((item) => item.id === payload);
      return { ...state, currentPage: page };

    case constants.SET_CURRENT_PAGE: {
      const section = payload.split("/")[1];
      const page = payload;
      const mainMenu = state.menuItem.find((item) => item.attr === section);
      const subMenu = mainMenu.dropDown.find((item) => item.link === `${page}`);
      return { ...state, currentPage: subMenu };
    }
    case constants.SET_NOT_FOUND_MENU_BG: {
      const notFound = { id: 0, background: payload };
      return { ...state, currentPage: notFound };
    }
    case constants.SET_MENU_BG:
      console.log(payload);
      return state;
    default:
      return state;
  }
}
