import { combineReducers } from "@reduxjs/toolkit";
import leadership from "./leadership";
import menu from "./menu";

export default combineReducers({ menu, leadership });
